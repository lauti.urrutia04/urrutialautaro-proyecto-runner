﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> powerUps;
    public GameObject instantiatePos;
    public float respawningTimer;
    public float respawnTimerPU; // timer para los powerups
    private float time = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
    }

    void Update()
    {
        SpawnEnemies();
        SpawnPowerUps();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }
    }

    private void SpawnPowerUps()
    {
        respawnTimerPU -= Time.deltaTime;

        if (respawnTimerPU <= 0)
        {
            Instantiate(powerUps[UnityEngine.Random.Range(0, powerUps.Count)], instantiatePos.transform);
            respawnTimerPU = UnityEngine.Random.Range(6, 12);
        }
    }
}
