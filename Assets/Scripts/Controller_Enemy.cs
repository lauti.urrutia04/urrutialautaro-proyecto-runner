﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    public bool doesMoveY = false;
    private bool doesMoveUp = false;
    private float minHeight = 0;
    private float maxHeight = 5;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();

        //Enemigo con movimiento vertical
        if (doesMoveY)
        {
            if (transform.position.y <= minHeight)
            {
                doesMoveUp = true;
            }
            else if (transform.position.y >= maxHeight)
            {
                doesMoveUp = false;
            }

            if (doesMoveUp)
            {
                MoveUp();
            }
            else
            {
                MoveDown();
            }
        }
    }
    //Movimiento arriba
    private void MoveUp()
    {
        transform.position += transform.up * enemyVelocity / 2 * Time.deltaTime;
    }
    //Movimiento abajo
    private void MoveDown()
    {
        transform.position -= transform.up * enemyVelocity * Time.deltaTime;
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
