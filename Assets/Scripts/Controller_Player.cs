﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    private Material material;
    public float jumpForce = 10;
    public float moveSpeed = 10;
    private float initialSize;
    private int i = 0;

    private bool floored;
    private bool invincible;
    public float baseTimerInvincible = 6f;
    public float timerInvincible;

    //limites de movimiento horizontal
    public float xMaxMovement = 20f;
    public float xMinMovement = -10f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        material = GetComponent<Material>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();
        
        if (invincible)
        {
            timerInvincible -= Time.deltaTime;
            if (timerInvincible <= 0)
            {
                invincible = false;
            }
        }
    }

    private void GetInput()
    {
        Jump();
        Duck();
        Move();
    }

    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    //Movimiento hacia abajo
    private void Duck()
    {
        //agacharse
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        //envión hacia abajo
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    //movimiento horizontal
    private void Move()
    {
        float XAxisMove = Input.GetAxis("Horizontal") * moveSpeed;
        XAxisMove *= Time.deltaTime;

        transform.Translate(XAxisMove, 0, 0);

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, xMinMovement, xMaxMovement), transform.position.y, transform.position.z);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (!invincible) // comprobación de invencibilidad
            {
                Destroy(this.gameObject);
                Controller_Hud.gameOver = true;
                Parallax.isAlive = false;
            }
            else if (invincible)
            {
                Destroy(collision.gameObject);
                invincible = false;
            }
        }

        if (collision.gameObject.CompareTag("PowerUp"))
        {
            Destroy(collision.gameObject);
            timerInvincible = baseTimerInvincible;
            invincible = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}
