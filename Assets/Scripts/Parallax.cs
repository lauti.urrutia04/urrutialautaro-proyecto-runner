﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;
    //Velocidad del Parallax general:
    public static float parallaxSpeed;
    [HideInInspector]
    public static bool isAlive = true;

    void Start()
    {
        parallaxSpeed = 0.7f;
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        //comprobar que el jugador esté vivo
        if (isAlive)
        {
            MoveParallax();
        }
        
    }

    void MoveParallax()
    {
        transform.position = new Vector3(transform.position.x - parallaxEffect * parallaxSpeed, transform.position.y, transform.position.z);
        if (transform.localPosition.x < -20)
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
        }
    }
}
