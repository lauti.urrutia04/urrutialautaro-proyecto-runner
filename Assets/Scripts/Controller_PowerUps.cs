using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PowerUps : MonoBehaviour
{
    public float speed;
    private bool movesUp = false;
    private float minHeight = 3;
    private float maxHeight = 4;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb.AddForce(new Vector3(-speed, 0, 0), ForceMode.Force);
        OutOfBounds();

        if (transform.position.y <= minHeight)
        {
            movesUp = true;
        }
        else if (transform.position.y >= maxHeight)
        {
            movesUp = false;
        }

        if (movesUp)
        {
            MoveUp();
        }
        else
        {
            MoveDown();
        }
    }

    private void MoveUp()
    {
        transform.position += transform.up * speed * Time.deltaTime;
    }
    private void MoveDown()
    {
        transform.position -= transform.up * speed * Time.deltaTime;
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
